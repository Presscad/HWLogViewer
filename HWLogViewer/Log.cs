using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HWLogViewer
{
    public class Log
    {
        /// <summary>
        /// Recoding time.
        /// </summary>
        public DateTime Time { get; set; } = DateTime.Now;

        /// <summary>
        /// The type of the log.
        /// </summary>
        public int LogType { get; set; }

        /// <summary>
        /// Full class name + method name, such as HWLogViewer.Log.Parse
        /// </summary>
        public string MethodInfo { get; set; }

        /// <summary>
        /// Arguements of a log.
        /// </summary>
        public string[] Arguments { get; set; }

        /// <summary>
        /// Load a list of logs from a log file.
        /// </summary>
        /// <param name="logfile">The input log file.</param>
        /// <returns></returns>
        public static List<Log> LoadFromFile(string logfile)
        {
            List<Log> logs = new List<Log>();
            if (File.Exists(logfile))
            {
                foreach (string line in File.ReadAllLines(logfile))
                {
                    if (line.Trim().Length == 0)
                        continue;

                    Log log = Log.Parse(line);
                    if (log != null)
                        logs.Add(log);
                }
            }

            return logs;
        }

        /// <summary>
        /// To a string array: {time, log type, method name, arguments} 
        /// </summary>
        /// <returns></returns>
        public string[] ToStrings()
        {
            string[] data = new string[Arguments.Length + 3];
            data[0] = this.Time.ToString("yyyy/MM/dd HH:mm:ss");
            data[1] = this.LogType.ToString(); 
            data[2] = this.MethodInfo;  
            for (int i = 0; i < Arguments.Length; i++)
                data[i + 3] = Arguments[i];
            return data;
        }


        // Used to separate data fields of a log when serializing.
        static char separator = (char)6;
        // to replace \n with this
        static string lineBreaker = "a@c3k_K4.X@a-3;p";
        // start time to compare with
        static DateTime startTime = new DateTime(2019, 1, 1);

        /// <summary>
        /// Format: yyyyMMddHHmmss|LogType|Arguments.
        /// Note: "\r\n" or "\n" --> "a@c3k_K4.X@a-3;p"
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append((int)(Time - startTime).TotalSeconds + separator);
            sb.Append((int)LogType + separator);
            sb.Append(MethodInfo + separator);
            sb.Append(string.Join(separator.ToString(), Arguments));
            return sb.ToString().Replace("\r", "").Replace("\n", lineBreaker);
        }

        /// <summary>
        /// Parses a string into a Log object. If the format of input string is incorrect, returns null.
        /// </summary>
        /// <param name="str">Input string.</param>
        /// <returns></returns>
        public static Log Parse(string str)
        {
            Log log = null; 
            try
            {
                string[] data = str.Replace(lineBreaker, "\r\n").Split(separator);
                log = new Log
                {
                    Time = startTime.AddSeconds(int.Parse(data[0])),
                    LogType = int.Parse(data[1]),
                    MethodInfo = data[2],
                    Arguments = new string[data.Length - 3]
                };

                for (int i = 0; i < log.Arguments.Length; i++)
                    log.Arguments[i] = data[i + 3];
            }catch{ }

            return log;
        }
    }
}
